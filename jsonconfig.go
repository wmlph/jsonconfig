package jsonconfig

import (
	"encoding/json"
	"io/ioutil"
)

func LoadConfig(filename string, output interface{}) (err error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	err = json.Unmarshal(b, output)
	return
}

func SaveConfig(filename string, data interface{}) (err error) {
	b, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return
	}
	err = ioutil.WriteFile(filename, b, 0644)
	return
}
